module gitlab.oit.duke.edu/ddr/morticia

go 1.20

require (
	github.com/jung-kurt/gofpdf v1.16.2
	github.com/yl2chen/cidranger v1.0.2
	gopkg.in/gographics/imagick.v2 v2.7.0
	gopkg.in/gographics/imagick.v3 v3.7.0
)
