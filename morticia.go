package main

import (
	"bufio"
	"encoding/json"
	"fmt"
	"net"
	"os"
	"strings"

	"github.com/yl2chen/cidranger"
)

func main() {
	// read the data
	data, _ := os.ReadFile("./tencent.json")
	var ipRanges []interface{}
	json.Unmarshal(data, &ipRanges)

	// construct the ranger
	ranger := cidranger.NewPCTrieRanger()

	// insert the data
	var network *net.IPNet
	for _, ipRange := range ipRanges {
		_, network, _ = net.ParseCIDR(ipRange.(string))
		ranger.Insert(cidranger.NewBasicRangerEntry(*network))
	}

	// query the data
	// docker-compose -f --no-log-prefix web | morticia
	var line string
	var ip net.IP
	var found bool
	scanner := bufio.NewScanner(os.Stdin)
	for scanner.Scan() {
		line = scanner.Text()
		ip = net.ParseIP(strings.SplitN(line, " ", 2)[0])
		found, _ = ranger.Contains(ip)
		if found {
			fmt.Println(line)
		}
	}
}
