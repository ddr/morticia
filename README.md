# morticia

Track web traffic from Tencent IP ranges.

OKD:

    $ oc logs -f front-end-c8446785-bbddq | ./morticia

Docker Compose:

    $ docker-compose -f --no-log-prefix web | ./morticia
